#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    char *hashedguess = md5(guess, strlen(guess));
    int match = strcmp(hashedguess, hash);
    free(hashedguess);
    
    if(match == 0)
    {
        return 1;
    }
    
    else
    {
        return 0;
    }
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    int lines = 10;
    char **guesses = malloc(lines * sizeof(char *));
    char line[1000];
    int count = 0;
    
    FILE *dictfile = fopen(filename, "r");
    while(fgets(line, 1000, dictfile))
    {
        if(count == lines)
        {
            lines += 10;
            guesses = realloc(guesses, lines * sizeof(char *));
        }
        
        line[strlen(line) - 1] = '\0';
        char *guess = malloc(strlen(line) * sizeof(char) + 1);
        strcpy(guess, line);
        guesses[count] = guess;
        count ++;
        
    }
    
    *size = count;
    fclose(dictfile);
    return guesses;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    //Test Readability of Dictionary File
    FILE *dictfile = fopen(argv[2], "r");
    if(!dictfile)
    {
        printf("Unable to open %s for reading\n", argv[2]);
        return 2
    }
    fclose(dictfile);

    // Read the dictionary file into an array of strings.
    int dlen;
    char **dict = read_dictionary(argv[2], &dlen);

    // Open the hash file for reading.
    FILE *hashfile = fopen(argv[1], "r");
    if(!hashfile)
    {
        printf("Unable to open %s for reading\n", argv[1]);
        return 3;
    }

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    char hash[1000];
    int hashnum = 0;
    
    while(fgets(hash, 1000, hashfile))
    {
        hash[strlen(hash) -1] = '\0';
        hashnum++;
        int foundMatch = 0;
        
        for(int i = 0; i < dlen; i++)
        {
            int result = tryguess(hash, dict[i]);
            if(result == 1)
            {
                printf("%d %s %s\n", hashnum, hash, dict[i]);
                foundMatch = 1;
                break;
            }
        }
        
        if(foundMatch == 0)
        {
            printf("%d %s --No Match Found-- \n", hashnum, hash);
        }
        
    }
    
    for(int i = 0; i < dlen; i++)
    {
        free(dict[i]);
    }
    
    free(dict);
    fclose(hashfile);
}
